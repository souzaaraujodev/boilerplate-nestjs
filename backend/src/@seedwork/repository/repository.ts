import { Logger } from '@nestjs/common';
import { ConfigDb } from 'src/configs/database/connection';

export abstract class Repository {
  protected dbClient: any;
  protected model: string;
  protected prefix: string;
  public readonly logger = new Logger(Repository.name);

  constructor(model: string, prefix: string) {
    ConfigDb.connect().then((client) => (this.dbClient = client));
    this.model = model;
    this.prefix = prefix;
  }

  async getByPrimaryKey(param: string): Promise<any> {
    this.logger.log(`Get By Primary Key: ${param}`);

    const result = await this.dbClient[this.model].findFirst({
      where: {
        [`${this.prefix}_id`]: param,
        [`${this.prefix}_deleted_at`]: null,
      },
    });

    if (!result) {
      return null;
    }

    return result;
  }
}
