import { randomUUID } from 'crypto';
import { isEmpty as empty } from 'lodash';

/**
 * Utils Class to provide some useful functions
 * @date 5/22/2023 - 9:16:31 AM
 *
 * @export
 * @class Utils
 * @typedef {Utils}
 */
export class Utils {
  static isEmpty = (value: any) => empty(value);
  static uuid = () => randomUUID();
}
