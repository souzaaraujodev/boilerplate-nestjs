export class AppError extends Error {
  public readonly name: string;
  public readonly message: string;
  public readonly cause: any;
  public readonly code: number;

  constructor(message: string, cause: any, code: number) {
    super(message);
    this.name = 'AppError';
    this.cause = cause;
    this.code = code;
  }
}
