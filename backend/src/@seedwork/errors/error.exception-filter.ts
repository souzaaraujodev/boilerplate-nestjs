import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { AppError } from './app-error';

@Catch(Error)
export default class ErrorExceptionFilter implements ExceptionFilter {
  catch(exception: AppError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();

    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();

    const payload = {
      message: exception.message,
      timestamp: new Date().toISOString(),
      path: request.url,
    };

    let status: number = exception?.code ?? HttpStatus.BAD_REQUEST;

    if (exception instanceof HttpException) {
      const error = exception.getResponse() as any;

      payload.message = error.message;
      status = exception.getStatus();
    }

    response.status(status).send(payload);
  }
}
