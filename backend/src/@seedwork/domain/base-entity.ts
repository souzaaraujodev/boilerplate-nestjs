import { Utils } from '../utils/utils';

export default abstract class BaseEntity<Props = any> {
  public readonly id: string;

  constructor(public readonly props: Props, id?: string) {
    this.id = id || Utils.uuid();
  }

  toJSON(): Required<{ id: string } & Props> {
    return {
      id: this.id,
      ...this.props,
    } as Required<{ id: string } & Props>;
  }
}
