import { NestFactory } from '@nestjs/core';
import { AppModule } from './modules/app.module';
import { createSwaggerDoc } from './configs/swagger/create-swagger-doc';
import { ConfigService } from '@nestjs/config';

const DOC_ROUTE = 'docs';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const API_PORT = configService.get('PORT');
  await createSwaggerDoc(app);
  await app.listen(API_PORT || 3000, '0.0.0.0', async () => {
    console.log(`RUNNING AT: ${await app.getUrl()}`);
    console.log(`DOCS: ${await app.getUrl()}/${DOC_ROUTE}`);
  });
}
bootstrap();
