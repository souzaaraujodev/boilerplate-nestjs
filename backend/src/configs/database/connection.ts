import { PrismaClient } from '@prisma/client';

/**
 * @description - This class is used to connect to the database
 * @date 9/19/2023 - 8:50:36 AM
 *
 * @export
 * @class ConfigDb
 * @typedef {ConfigDb}
 */
export class ConfigDb {
  public static async connect(): Promise<PrismaClient> {
    return new PrismaClient();
  }
}
