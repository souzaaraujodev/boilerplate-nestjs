import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { swaggerRegisterModules } from './swagger-register-modules';

export const createSwaggerDoc = async (app: INestApplication) => {
  const document = createDocument(app);

  SwaggerModule.setup('/docs', app, document);
};

export const createDocument = function (app: INestApplication) {
  const config = new DocumentBuilder()
    .setTitle('Project')
    .setDescription('Documentação dos endpoints')
    .setVersion('1.0')
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'authorization',
    )
    .addBasicAuth({ type: 'http', scheme: 'basic' }, 'basic')
    .build();

  return SwaggerModule.createDocument(app, config, {
    include: swaggerRegisterModules,
  });
};
