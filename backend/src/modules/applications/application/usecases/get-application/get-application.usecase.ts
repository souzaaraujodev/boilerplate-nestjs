import { ApplicationRepository } from 'src/modules/applications/infra/db/repository/application.repository';
import { GetApplicationInput } from '../../dto/get-application-input.dto';
import { ApplicationMapper } from 'src/modules/applications/infra/db/mappers/application.mapper';
import { GetApplicationOutput } from '../../dto/get-application-output.dto';

export class GetApplicationUseCase {
  constructor(private readonly repository: ApplicationRepository) {}

  async execute(
    applicationInput: GetApplicationInput,
  ): Promise<GetApplicationOutput> {
    const dbItem = await this.repository.getByPrimaryKey(applicationInput.id);
    const application = ApplicationMapper.databaseToApplication(dbItem);
    return {
      id: application.id,
      title: application.props.title,
      description: application.props.description,
    } as GetApplicationOutput;
  }
}
