export type GetApplicationOutput = {
  id: string;
  title: string;
  description: string;
};
