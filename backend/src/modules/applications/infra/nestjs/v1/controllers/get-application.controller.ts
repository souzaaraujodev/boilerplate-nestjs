import {
  Controller,
  Get,
  HttpCode,
  HttpException,
  HttpStatus,
  Param,
} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { GetApplicationInput } from 'src/modules/applications/application/dto/get-application-input.dto';
import { GetApplicationUseCase } from 'src/modules/applications/application/usecases/get-application/get-application.usecase';

@Controller('internal/applications')
@ApiBearerAuth('authorization')
@ApiTags('Applications')
export class GetApplicationController {
  constructor(private readonly getApplicationUseCase: GetApplicationUseCase) {}

  @ApiOperation({
    description: 'Buscar uma aplicacao',
  })
  @HttpCode(HttpStatus.OK)
  @Get(':id')
  async getApplication(@Param('id') id: string) {
    const application = await this.getApplicationUseCase.execute({
      id,
    } as GetApplicationInput);

    if (!application)
      throw new HttpException(
        'Application not available',
        HttpStatus.NOT_FOUND,
      );

    return application;
  }
}
