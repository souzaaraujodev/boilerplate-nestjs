import { Module } from '@nestjs/common';
import { ApplicaitionFactories } from './application.factories';
import { GetApplicationController } from './controllers/get-application.controller';

@Module({
  imports: [],
  controllers: [GetApplicationController],
  providers: ApplicaitionFactories,
})
export class ApplicationModule {}
