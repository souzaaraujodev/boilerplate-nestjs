import { ApplicationRepository } from '../../db/repository/application.repository';
import { Provider } from '@nestjs/common/interfaces/modules/provider.interface';
import { GetApplicationUseCase } from 'src/modules/applications/application/usecases/get-application/get-application.usecase';

export const ApplicaitionFactories: Provider[] = [
  {
    provide: ApplicationRepository.name,
    useClass: ApplicationRepository,
  },

  {
    provide: GetApplicationUseCase,
    useFactory: (repository: ApplicationRepository) =>
      new GetApplicationUseCase(repository),
    inject: [ApplicationRepository.name],
  },
];
