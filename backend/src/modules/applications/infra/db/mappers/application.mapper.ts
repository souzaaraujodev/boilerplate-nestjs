import { Application } from 'src/modules/applications/domain/entities/application';
import { DateTime } from 'luxon';

export class ApplicationMapper {
  public static applicationToDatabase(data: Application): any {
    return {
      app_id: data.props.id,
      app_title: data.props.title,
      app_description: data.props.description,
      app_created_at: DateTime.now().set({}).toUTC().toISO(),
      app_updated_at: DateTime.now().set({}).toUTC().toISO(),
    };
  }

  public static databaseToApplication(data: any): Application {
    return new Application({
      id: data.app_id,
      title: data.app_title,
      description: data.app_description,
    });
  }
}
