import { Repository } from 'src/@seedwork/repository/repository';

export class ApplicationRepository extends Repository {
  constructor() {
    super('application', 'app');
  }
}
