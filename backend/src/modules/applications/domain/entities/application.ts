import BaseEntity from 'src/@seedwork/domain/base-entity';
import { ApplicationProps } from '../types/application.type';

export class Application extends BaseEntity<ApplicationProps> {
  constructor(public readonly props: ApplicationProps, id?: string) {
    super(props, id);
    this.sanitize();
  }

  private sanitize() {
    this.props.title = this.props.title.trim();
    this.props.description = this.props.description.trim();
  }
}
