export type ApplicationProps = {
  id: string;
  title: string;
  description: string;
};
