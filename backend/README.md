# Backend Boilerplate Application

# Install

`yarn` && `yarn start:dev`.

# Clean Arch

- `prisma`: diretório para armazenar os schemas do client prisma;
- `src/@seedwork/domain`: objetos de dominio da aplicacao;
- `src/@seedwork/errors`: controle de handlers de erros do framework e classes de mapper de erros;
- `src/@seedwork/repository`: classes bases para extender o repository (database);
- `src/@seedwork/utils`: metodos para auxiliar durante o desenvolvimento;
- `src/@seedwork/configs`: configuracoes da aplicacão como client database e docs de swagger;
- `src/@seedwork/modules`: modulos da aplicacao que devem ser plugados no app.module.ts;

# Autor

Leandro de Souza Araujo
leandro.souara.web@gmail.com
67 99160-4334
